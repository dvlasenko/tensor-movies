//
//  MoviesPresenter.swift
//  tensor-movies
//
//  Created by vlazze on 23.08.2021.
//

import Foundation

protocol MoviesPresenterProtocol: AnyObject {
  func fetchData(callback: @escaping () -> ())
  func getMoviesCount() -> Int
  func getMovie(byIndex index: Int) -> MovieModel
}

protocol MoviesPresenterDelegate: AnyObject {}

class MoviesPresenter: MoviesPresenterProtocol {
    private var api: MoviesApiProtocol
    private var movies = [MovieModel]()
    private weak var delegate: MoviesPresenterDelegate?
    
    init(service: MoviesApiProtocol, delegate delegateParam: MoviesPresenterDelegate?) {
        api = service
        delegate = delegateParam
    }
    
    func fetchData(callback: @escaping () -> ()) {
        api.getMoviesData { [weak self] (result) in
            switch result {
            case .success(let listOf):
                self?.movies = listOf.movies
                callback()
            case .failure(let error):
                print("JSON Error: \(error)")
            }
        }
    }
    
    func getMoviesCount() -> Int {
        return movies.count
    }
    
    func getMovie(byIndex index: Int) -> MovieModel {
        return movies[index]
    }
}
