//
//  MoveModel.swift
//  tensor-movies
//
//  Created by vlazze on 23.08.2021.
//

import Foundation

struct MovieModel: Decodable {
    let title: String?
    let rating: Float?
    let image: String?
    
    private enum CodingKeys: String, CodingKey {
        case title
        case rating = "vote_average"
        case image = "poster_path"
    }
}
