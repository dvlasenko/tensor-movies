//
//  MoviesModel.swift
//  tensor-movies
//
//  Created by vlazze on 23.08.2021.
//

import Foundation

struct MoviesModel: Decodable {
    let movies: [MovieModel]
    
    private enum CodingKeys: String, CodingKey {
        case movies = "results"
    }
}
