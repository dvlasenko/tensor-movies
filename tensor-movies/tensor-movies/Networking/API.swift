//
//  API.swift
//  tensor-movies
//
//  Created by vlazze on 23.08.2021.
//

import Foundation

class API: MoviesApiProtocol {
    private var dataTask: URLSessionDataTask?
    
    func getMoviesData(callback: @escaping (Result<MoviesModel, Error>) -> Void) {
        let methodUrl = "https://api.themoviedb.org/3/movie/popular";
        let apiKey = "api_key=5e0f11fece36bf24dbbbaca096353875"
        let lang = "language=ru-RU"
        let page = "page=1"
        let completedUrl = "\(methodUrl)?\(apiKey)&\(lang)&\(page)"
        
        guard let url = URL(string: completedUrl) else { return }
        
        dataTask = URLSession.shared.dataTask(with: url) { (data, responce, error) in
            if let error = error {
                callback(.failure(error))
                print("Error: \(error.localizedDescription)")
            }
            
            guard let responce = responce as? HTTPURLResponse else {
                print("Empty response")
                return
            }
            
            print("Response: \(responce.statusCode)")
            
            guard let data = data else {
                print("Empty data")
                return
            }
            
            do {
                let decoder = JSONDecoder()
                let jsonData = try decoder.decode(MoviesModel.self, from: data)
                
                DispatchQueue.main.async {
                    callback(.success(jsonData))
                }
            } catch let error {
                callback(.failure(error))
            }
        }
        
        dataTask?.resume()
    }
}
