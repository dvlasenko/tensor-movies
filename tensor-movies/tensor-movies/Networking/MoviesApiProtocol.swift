//
//  MoviesApiProtocol.swift
//  tensor-movies
//
//  Created by vlazze on 24.08.2021.
//

import Foundation

protocol MoviesApiProtocol: AnyObject {
    func getMoviesData(callback: @escaping (Result<MoviesModel, Error>) -> Void)
}
