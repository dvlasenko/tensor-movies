//
//  ViewController.swift
//  tensor-movies
//
//  Created by vlazze on 21.08.2021.
//

import UIKit

class MoviesViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var presenter: MoviesPresenterProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        let service = API()
        presenter = MoviesPresenter(service: service, delegate: self)
        presenter?.fetchData { [weak self] in
            self?.tableView.dataSource = self
            self?.tableView.reloadData()
        }
    }
}

extension MoviesViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter?.getMoviesCount() ?? .zero
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MovieTableViewCell
        guard let movie = presenter?.getMovie(byIndex: indexPath.row) else { return cell }
        
        cell.setCellWithValuesOf(movie)
        
        return cell
    }
}

extension MoviesViewController: MoviesPresenterDelegate {}

