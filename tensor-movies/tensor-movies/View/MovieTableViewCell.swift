//
//  MovieTableViewCell.swift
//  tensor-movies
//
//  Created by vlazze on 23.08.2021.
//

import UIKit

class MovieTableViewCell: UITableViewCell {
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var rating: UILabel!
    @IBOutlet weak var posterImage: UIImageView!
    
    private var urlString: String = ""
    
    func setCellWithValuesOf(_ values: MovieModel) {
        updateCell(title: values.title,
                   rating: values.rating,
                   posterImage: values.image)
    }
    
    func updateCell(title newTitle: String?,
                    rating newRating: Float?,
                    posterImage newPosterImage: String?) {
        title.text = newTitle
        
        guard let ratingUnwrapped = newRating else { return }
        rating.text = String(ratingUnwrapped)
        
        guard let posterString = newPosterImage else { return }
        let urlString = "https://image.tmdb.org/t/p/w300\(posterString)"
        
        guard let posterImageURL = URL(string: urlString) else {
            posterImage.image = UIImage(systemName: "photo")
            return
        }
        
        posterImage.image = nil
        
        getImageData(url: posterImageURL)
    }
    
    private func getImageData(url: URL) {
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let error = error {
                print("Error: \(error.localizedDescription)")
                return
            }
            
            guard let data = data else {
                print("Empty data")
                return
            }
            
            DispatchQueue.main.async {
                if let image = UIImage(data: data) {
                    self.posterImage.image = image
                }
            }
        }.resume()
    }
}
